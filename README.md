# Broderick illusory grating

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.1297171.svg)](https://doi.org/10.5281/zenodo.1297171)

Code to create a Broderick illusory grating, see [this blog post](https://wfbroderick.com/accidental-illusion.html)

An interactive bokeh plot is included in order to explore the grating,
which can be run as a standalone server.

~~An interactive deployment of this can be found at [the author's website](https://wfbroderick.com/illusory-grating/main)~~. As of May 2024, I'm no longer hosting this app on my website. Open an issue if you have issues getting it to run!

## Screenshots

The app allows you to control the various parameters that influence the appearance of the illusion. The visibility of the illusion is determined by the relationship between the size of the circular apertures and the spatial frequency of the underlying grating.

In the following image, the illusory grating is visible:

![illusion!](assets/just-right.png)

Whereas in this image, the apertures are too small, and you thus only see the underlying grating:

![illusion!](assets/too-small.png)

And in this one, the apertures are too big:

![illusion!](assets/too-big.png)

## Requirements

You'll need python 2.7 (this was created using python 2.7.12; python 3
will probably work but is not guaranteed) and the packages found in
the `requirements.txt` (you can install all of these by running `pip
install -r requirements.txt` from the command line). You should
probably create a new virtual environment to do this, because we
require a specific version of `tornado` (tornado >= 4.5 seems to
break).

This was created on Linux and so will probably also work on OSX
without any problems, but there are no guarantees for Windows.

If none of the above makes sense to you, then make sure you've got
python 2.7 installed (you can get it
via [Anaconda](https://www.anaconda.com/download/)), open the command
line, navigate to where you've downloaded this folder, and do the
following (this will only need to be done once):

```
virtualenv grating-reqs
source grating-reqs/bin/activate
pip install -r requirements.txt
```

And then continue to usage. When you've finished, type
`deactivate`. In the future, when you want to run this, you'll need to
first type `source grating-reqs/bin/activate`

This will create a directory in this folder called `grating-reqs`, and
all the required packages will be installed within that. Typing the
`source` command above change your current version of python to the
one installed within that folder (a "virtual environment") so you have
access to all those packages. Typing `deactivate` at the end returns
it to the default version. This usage of virtual environments to
manage packages is considered best practice in python. If you're done
with the virtual environment you installed, you can simply remove it
by deleting the folder.

## Usage

To create a grating with apertures overlaid, run:
`grating.create_windowed_grating(spatial_frequency, orientation,
aperture_size)` (there are other optional parameters). To see the
illusion, try `spatial_frequency=15`, `orientation=70`,
`aperture_size=23`.

In order to run the interactive Bokeh server and explore all the
possibilities, just run `bokeh serve --show main.py` from within this
directory.
