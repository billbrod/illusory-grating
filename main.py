# illusory_grating.py
"""helper functions to create illusory grating
"""

import grating
import pandas as pd
import numpy as np
from scipy import fftpack
from bokeh.plotting import figure, curdoc
from bokeh.layouts import row, widgetbox
from bokeh.models import ColumnDataSource
from bokeh.models.widgets import Slider, RadioButtonGroup, Button


def modify_doc(doc):
    # Set up data
    size = 1000

    # Create images
    apertured_sin = grating.create_windowed_grating(15, 70, 23, size=size)
    source = ColumnDataSource(data=dict(image=[apertured_sin]))

    # Set up plot
    plot = figure(title='Illusory grating', plot_height=800, plot_width=800, x_range=(0, size),
                  y_range=(0, size), tools="save")
    plot.image(image='image', source=source, x=0, y=0, dh=size, dw=size)
    plot.xaxis.visible = False
    plot.yaxis.visible = False
    plot.toolbar.active_drag = None

    # Set up widgets
    size_slider = Slider(title='Grating size (pixels)', value=size, start=100, end=1000, step=10)
    sf = Slider(title="Spatial frequency (cycles/image)", value=15, start=0, end=100, step=1)
    orientation = Slider(title="Orientation (degrees)", value=70, start=0, end=180, step=1)
    phase = Slider(title='Phase (degrees)', value=0, start=0, end=180, step=1)
    aperture_size = Slider(title='Aperture radius (pixels)', value=23, start=0, end=100, step=1)
    extra_aperture_spacing = Slider(title="Pixels between apertures", value=0, start=0, end=100)
    aperture_mode = RadioButtonGroup(labels=['Circular Aperture', 'Gaussian Aperture'], active=0)
    aperture_spacing = RadioButtonGroup(labels=['Square Grid', 'Hexagonal Grid'], active=0)
    # image_mode = RadioButtonGroup(labels=['Grating', 'Contrast', 'Fourier Transform'], active=0)
    image_mode = RadioButtonGroup(labels=['Grating', 'Contrast'], active=0)
    gabor_freq = Slider(title='Frequency of Gabor filter', value=.2, start=.1, end=1, step=.1)
    submit_button = Button(label='Submit parameters', button_type='primary')

    # Set up callbacks
    def on_change(attrname, old, new):

        # get current slider values
        size = size_slider.value
        spatial_frequency_value = sf.value
        orientation_value = orientation.value
        phi = phase.value
        m = aperture_size.value
        es = extra_aperture_spacing.value
        mode = ['circle', 'gaussian'][aperture_mode.active]
        spacing = ['square', 'hexagonal'][aperture_spacing.active]
        submit_button.label = "Submit parameters"
        submit_button.button_type = 'primary'
        gabor_freq_value = gabor_freq.value

        # generate new image
        apertured_sin = grating.create_windowed_grating(spatial_frequency_value, orientation_value,
                                                        m, phi, size, es, mode, spacing)

        if image_mode.active == 0:
            source.data = dict(image=[apertured_sin])
        elif image_mode.active == 1:
            source.data = dict(image=[grating.get_contrast(apertured_sin, gabor_freq_value)])
        elif image_mode.active == 2:
            source.data = dict(image=[np.abs(fftpack.fft2(apertured_sin))])


    def on_click(new):
        on_change(None, None, new)


    def submit_params():
        df = pd.read_csv("params.csv")
        tmp = pd.DataFrame({'size': size_slider.value, "spatial_frequency": sf.value,
                            'orientation': orientation.value, 'phase': phase.value,
                            'aperture_radius': aperture_size.value, 
                            'extra_aperture_spacing': extra_aperture_spacing.value,
                            'aperture_mode': ['circle', 'gaussian'][aperture_mode.active],
                            'aperture_spacing_mode': ['square', 'hexagonal'][aperture_spacing.active]},
                           index=[0])
        df = df.append(tmp).reset_index(drop=True)
        df.to_csv("params.csv", index=False)
        submit_button.label = 'Thanks!'
        submit_button.button_type = 'success'


    for attrs in [size_slider, sf, orientation, phase, aperture_size, extra_aperture_spacing,
                  gabor_freq]:
        attrs.on_change('value', on_change)

    aperture_mode.on_click(on_click)
    aperture_spacing.on_click(on_click)
    submit_button.on_click(submit_params)
    image_mode.on_click(on_click)

    # Set up layouts and add to document
    inputs = widgetbox(size_slider, sf, orientation, phase, aperture_size, extra_aperture_spacing,
                       aperture_mode, aperture_spacing, submit_button, )#image_mode, gabor_freq)

    doc.add_root(row(inputs, plot, width=800))
    doc.title = "Illusory grating"
    
    return doc

doc = modify_doc(curdoc())
