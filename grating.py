# grating.py
#
# functions to actually create the grating


import math
import numpy as np
from scipy import signal, stats
from skimage.filters import gabor_kernel


def create_sin(size, w_x, w_y, phase=0):
    """create a full 2d sine wave, with frequency in cycles / image

    phase should be in radians.
    """
    x = np.array(range(1, size+1))
    size = float(size)
    x, y = np.meshgrid(x, x)
    return np.cos(2*np.pi*x*(w_x/size) + 2*np.pi*y*(w_y/size) + phase)


def create_sin_wrapper(size, spatial_frequency, orientation, phase=0):
    """create a full 2d sine wave

    phase and orientation should be in degrees

    spatial_frequency should be in cycles / image
    """
    orientation = math.radians(orientation)
    phase = math.radians(phase)
    w_x = spatial_frequency * np.cos(orientation)
    w_y = spatial_frequency * np.sin(orientation)
    return create_sin(size, w_x, w_y, phase)


def create_circle_aperture(x, y, rad, size, mode='circle'):
    """create a circular aperture

    this returns a circular aperture centered at pixel (x, y) with radius rad in a size by size
    image. This can then be multiplied by an image of the same size to aperture out everything
    else.

    mode: {'circle', 'gaussian'}, whether aperture should be circular or Gaussian (in which case
    the sigma is one half the supplied radius).
    """
    x_grid = np.array(range(size))
    x_grid, y_grid = np.meshgrid(x_grid, x_grid)
    aperture = np.zeros((size, size))
    if mode == 'circle':
        aperture[(x_grid - x)**2 + (y_grid - y)**2 <= rad**2] = 1
    elif mode == 'gaussian':
        gauss = stats.multivariate_normal(mean=[0, 0], cov=[[1, 0], [0, 1]])
        gauss_x, gauss_y = np.meshgrid(np.linspace(-2, 2, rad*2), np.linspace(-2, 2, rad*2))
        pos = np.empty(gauss_x.shape + (2,))
        pos[:, :, 0] = gauss_x
        pos[:, :, 1] = gauss_y
        gauss = gauss.pdf(pos)
        gauss /= gauss.max()
        aperture[y, x] = 1
        aperture = signal.fftconvolve(gauss, aperture, mode='same')
    else:
        raise Exception("Don't know how to handle mode %s" % mode)
    return aperture


def create_apertures(size, aperture_size, extra_aperture_spacing=0, aperture_mode='circle',
                     aperture_spacing_mode='square'):
    """create series of apertures

    aperture_size should be an integer, in number of pixels

    extra_aperture_spacing: int, extra pixels that should be placed between apertures

    aperture_spacing_mode: {'square', 'hexagonal'}, whether to place the masks along a square or
    (approximate) hexagonal lattice
    """
    apertures = np.zeros((size, size))
    aperture_spacing = 2 * (aperture_size + 1)
    aperture_spacing += extra_aperture_spacing
    if aperture_spacing_mode == 'square':
        apertures[aperture_size:size:aperture_spacing, aperture_size:size:aperture_spacing] = 1
    elif aperture_spacing_mode == 'hexagonal':
        height_offset = int((aperture_spacing / 2.) * np.tan(np.pi/3))
        apertures[aperture_size:size:2*height_offset, aperture_size:size:aperture_spacing] = 1
        apertures[aperture_size+height_offset:size:2*height_offset,
                  int(aperture_size + aperture_spacing/2.):size:aperture_spacing] = 1
    else:
        raise Exception("Don't know how to handle mode %s" % aperture_spacing_mode)
    single_aperture = create_circle_aperture(aperture_size, aperture_size, aperture_size,
                                             2 * (aperture_size + 1), aperture_mode)
    return signal.fftconvolve(apertures, single_aperture, mode='same')


def create_windowed_grating(spatial_frequency, orientation, aperture_size, phase=0, size=1000,
                            extra_aperture_spacing=0, aperture_mode='circle',
                            aperture_spacing_mode='square'):
    sin = create_sin_wrapper(size, spatial_frequency, orientation, phase)
    apertures = create_apertures(size, aperture_size, extra_aperture_spacing, aperture_mode,
                                 aperture_spacing_mode)
    return apertures * sin


def get_contrast(image, gabor_freq=.2, orientations=[np.pi/4, 3*np.pi/4]):
    """convolve with a Gabor in both directions, then square and sum outputs
    """
    filters = [gabor_kernel(gabor_freq, theta=th) for th in orientations]
    outputs = [signal.fftconvolve(image, f, mode='same') for f in filters]
    return np.abs(np.sqrt(np.sum([np.square(o) for o in outputs])))
